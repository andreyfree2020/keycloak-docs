# Keycloak

## Основы Keycloak

- [Зачем вообще нужен Keycloak](https://habr.com/ru/company/southbridge/blog/654475/)
- [Установка Keycloak на Ubuntu - 1](install-keycloak-sso-on-ubuntu-20-04/)
- [Установка Keycloak на Ubuntu - 2](https://itdraft.ru/2022/08/29/ustanovka-keycloak-i-postgesql-v-linux-centos-rocky-debian/)

## Работа с Реалмом
- [Настройка реалма в Keycloak](https://zzamzam.dev/posts/2022-03-16-keycloak-realm-settings/)
- [Google-авторизация в Keycloak](https://keycloakthemes.com/blog/how-to-setup-sign-in-with-google-using-keycloak)

## Кластеризация в Keycloak

- [Keycloak в режиме кластера - 1](https://www.keycloak.org/docs/18.0/server_installation/#_standalone-ha-mode)
- [Keycloak в режиме кластера - 2](https://www.keycloak.org/docs/18.0/server_installation/#_clustering)
- [Методы обнаружения группы кластера в Keycloak](https://www.keycloak.org/2019/05/keycloak-cluster-setup.html)